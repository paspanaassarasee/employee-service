# Employee-service

## To deploy with docker
* cd /path/to/project
* mvn clean package
* docker-compose build
* docker-compose up

## Swagger UI
* http://localhost:8080/swagger-ui.html

## Postman
* [environment](./localhost.postman_environment.json)
* [collection](./assignment-employee.postman_collection.json)