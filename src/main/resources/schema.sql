CREATE database if NOT EXISTS `employee`;

USE `employee`;

DROP TABLE IF EXISTS employees;
CREATE TABLE employees
(
    employee_id     INT auto_increment
        PRIMARY KEY,
    first_name      VARCHAR(255)                       NOT NULL,
    last_name       VARCHAR(255)                       NOT NULL,
    department_name VARCHAR(255)                       NOT NULL,
    create_date     datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    modify_date     datetime DEFAULT CURRENT_TIMESTAMP NOT NULL
);

INSERT INTO employees (first_name, last_name, department_name)
VALUES ('paspana', 'assarasee', 'information technology');




DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id       INT auto_increment
        PRIMARY KEY,
    email    VARCHAR(50)  NOT NULL,
    password VARCHAR(120) NOT NULL,
    username VARCHAR(20)  NOT NULL,
    CONSTRAINT users_email_uindex
        UNIQUE (email),
    CONSTRAINT users_username_uindex
        UNIQUE (username)
);