package com.assignment.employee.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class HeaderConstant {
    public final String AUTHORIZATION = "Authorization";
}

