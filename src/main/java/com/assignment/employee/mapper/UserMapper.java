package com.assignment.employee.mapper;

import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.entity.UsersEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = PasswordEncoderMapper.class)
public interface UserMapper {

    @Mapping(source = "password", target = "password", qualifiedBy = EncodedMapping.class)
    UsersEntity mapSignupRequestToUserEntity(SignupRequest request);

    SignupResponse mapUserEntityToSignupResponse(UsersEntity entity);

    @Mapping(target = "username", source = "entity.username")
    @Mapping(target = "email", source = "entity.email")
    @Mapping(target = "token", source = "token")
    LoginResponse mapEntityAndTokenToLoginResponse(UsersEntity entity, String token);
}
