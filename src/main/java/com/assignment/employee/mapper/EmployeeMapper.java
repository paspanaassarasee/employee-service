package com.assignment.employee.mapper;

import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.entity.EmployeesEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper
public interface EmployeeMapper {

    EmployeeResponse mapEntityToResponse(EmployeesEntity entity);

    EmployeesEntity mapSaveRequestToEntity(SaveEmployeeRequest request);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateEntityFromModifyRequest(ModifyEmployeeRequest request, @MappingTarget EmployeesEntity entity);
}
