package com.assignment.employee.controller;

import com.assignment.employee.dto.ErrorResponse;
import com.assignment.employee.dto.user.request.LoginRequest;
import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Tag(name = "user")
@RestController
@Setter(onMethod_ = @Autowired)
@Validated
@RequestMapping("/api/auth")
public class UserController {

    private UserService userService;

    @PostMapping("/signin")
    @Operation(summary = "signin one user")
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to signin one user",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = SignupResponse.class))
        ),
        @ApiResponse(
            responseCode  = "401",
            description  = "Fail to signin one user, Invalid username or password",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<LoginResponse> authenticateUser(
        @RequestBody @Valid LoginRequest request
    ) {
        return ResponseEntity.ok(userService.authenticateUser(request));
    }

    @PostMapping("/signup")
    @Operation(summary = "signup one user")
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to signup one user",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = SignupResponse.class))
        ),
        @ApiResponse(
            responseCode  = "400",
            description  = "Fail to signup one user, Username is already taken or Email is already in use",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<SignupResponse> registerUser(
        @RequestBody @Valid SignupRequest request
    ) {
        return ResponseEntity.ok(userService.registerUser(request));
    }
}
