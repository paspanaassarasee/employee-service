package com.assignment.employee.controller;

import com.assignment.employee.dto.ErrorResponse;
import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "employee")
@RestController
@Setter(onMethod_ = @Autowired)
@Validated
@RequestMapping("/api/employees")
public class EmployeeController {

    private EmployeeService employeeService;

    @GetMapping
    @Operation(summary = "retrieving all employee", security = { @SecurityRequirement(name = "bearer-key") })
    @ApiResponse(
        responseCode  = "200",
        description  = "Success to retrieving all employee",
        content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = EmployeeResponse.class)))
    )
    public ResponseEntity<List<EmployeeResponse>> getAllEmployee() {
        return ResponseEntity.ok(employeeService.getAllEmployee());
    }

    @GetMapping("/{id}")
    @Operation(summary = "retrieving one employee by ID", security = { @SecurityRequirement(name = "bearer-key") })
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to retrieving one employee by ID",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeResponse.class))
        ),
        @ApiResponse(
            responseCode  = "404",
            description  = "Fail to retrieving one employee by ID, Not found",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<EmployeeResponse> getEmployeeById(
        @PathVariable int id
    ) {
        return ResponseEntity.ok(employeeService.getEmployeeById(id));
    }

    @PostMapping
    @Operation(summary = "saving one employee", security = { @SecurityRequirement(name = "bearer-key") })
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to saving one employee",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeResponse.class))
        ),
        @ApiResponse(
            responseCode  = "400",
            description  = "Fail to saving one employee, Invalid require field",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<EmployeeResponse> saveEmployee(
        @RequestBody @Valid SaveEmployeeRequest request
    ) {
        return ResponseEntity.ok(employeeService.saveEmployee(request));
    }

    @PostMapping("/{id}")
    @Operation(summary = "modifying one employee", security = { @SecurityRequirement(name = "bearer-key") })
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to modifying one employee",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeResponse.class))
        ),
        @ApiResponse(
            responseCode  = "400",
            description  = "Fail to modifying one employee, Invalid require field",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        ),
        @ApiResponse(
            responseCode  = "404",
            description  = "Fail to modifying one employee, Not found",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<EmployeeResponse> modifyEmployee(
        @PathVariable int id,
        @RequestBody @Valid ModifyEmployeeRequest request
    ) {
        return ResponseEntity.ok(employeeService.modifyEmployee(id, request));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "deleting one employee by ID", security = { @SecurityRequirement(name = "bearer-key") })
    @ApiResponses(value = {
        @ApiResponse(
            responseCode  = "200",
            description  = "Success to deleting one employee by ID"
        ),
        @ApiResponse(
            responseCode  = "404",
            description  = "Fail to deleting one employee by ID, Not found",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))
        )
    })
    public ResponseEntity<?> deleteEmployeeById(
        @PathVariable int id
    ) {
        employeeService.deleteEmployeeById(id);
        return ResponseEntity.ok().build();
    }
}
