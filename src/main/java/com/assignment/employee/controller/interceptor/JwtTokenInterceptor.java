package com.assignment.employee.controller.interceptor;

import com.assignment.employee.constant.HeaderConstant;
import com.assignment.employee.service.JwtService;
import com.assignment.employee.service.UserService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Setter(onMethod_ = @Autowired)
@Slf4j
public class JwtTokenInterceptor implements HandlerInterceptor {

    private JwtService jwtService;
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        try {
            String jwtToken = parseJwt(request);
            if (jwtToken != null && jwtService.validateJwtToken(jwtToken)) {
                String username = jwtService.getUserNameFromJwtToken(jwtToken);
                userService.findByUsername(username);
                return true;
            }
        } catch (Exception e) {
            log.error("Unauthorized JwtToken", e);
        }

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return false;
    }

    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader(HeaderConstant.AUTHORIZATION);

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }

        return null;
    }
}
