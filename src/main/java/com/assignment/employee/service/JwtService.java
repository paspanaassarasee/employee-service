package com.assignment.employee.service;

public interface JwtService {

    String generateJwtToken(String username);

    String getUserNameFromJwtToken(String token);

    boolean validateJwtToken(String authToken);
}
