package com.assignment.employee.service;

import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;

import java.util.List;

public interface EmployeeService {

    EmployeeResponse getEmployeeById(int id);

    List<EmployeeResponse> getAllEmployee();

    void deleteEmployeeById(int id);

    EmployeeResponse saveEmployee(SaveEmployeeRequest request);

    EmployeeResponse modifyEmployee(int id, ModifyEmployeeRequest request);
}
