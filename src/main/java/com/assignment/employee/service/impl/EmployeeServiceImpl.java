package com.assignment.employee.service.impl;

import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.entity.EmployeesEntity;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.NotFoundException;
import com.assignment.employee.mapper.EmployeeMapper;
import com.assignment.employee.repository.EmployeesRepository;
import com.assignment.employee.service.EmployeeService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Setter(onMethod_ = @Autowired)
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeesRepository employeesRepository;
    private EmployeeMapper employeeMapper;

    @Override
    public List<EmployeeResponse> getAllEmployee() {

        log.info("getAllEmployee");

        List<EmployeesEntity> employeeEntities = employeesRepository.findAll();
        if (employeeEntities.isEmpty()) {
            return Collections.emptyList();
        }

        return employeeEntities
            .stream()
            .map(entity -> employeeMapper.mapEntityToResponse(entity))
            .collect(Collectors.toList());
    }

    @Override
    public EmployeeResponse getEmployeeById(int id) {

        log.info("getEmployeeById, {}", id);

        Optional<EmployeesEntity> optionalEmployee = employeesRepository.findById(id);

        return optionalEmployee
            .map(entity -> employeeMapper.mapEntityToResponse(entity))
            .orElseThrow(NotFoundException::new);
    }

    @Override
    public void deleteEmployeeById(int id) {

        log.info("deleteEmployeeById, {}", id);

        try {
            employeesRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new NotFoundException(e);
        }
    }

    @Override
    public EmployeeResponse saveEmployee(SaveEmployeeRequest request) {

        log.info("saveEmployee, {}", request);

        EmployeesEntity entity = employeeMapper.mapSaveRequestToEntity(request);
        EmployeesEntity responseEntity = employeesRepository.save(entity);

        return employeeMapper.mapEntityToResponse(responseEntity);
    }

    @Override
    public EmployeeResponse modifyEmployee(int id, ModifyEmployeeRequest request) {

        log.info("modifyEmployee, {} {}", id, request);

        boolean hasAnyDetailsEntered = request.hasAnyDetailsEntered();
        if (!hasAnyDetailsEntered) {
            throw new InvalidDataException("Invalid require field");
        }

        Optional<EmployeesEntity> optionalEmployee = employeesRepository.findById(id);

        return optionalEmployee
            .map(entity -> {
                employeeMapper.updateEntityFromModifyRequest(request, entity);
                EmployeesEntity responseEntity = employeesRepository.save(entity);
                return employeeMapper.mapEntityToResponse(responseEntity);
            })
            .orElseThrow(NotFoundException::new);
    }
}
