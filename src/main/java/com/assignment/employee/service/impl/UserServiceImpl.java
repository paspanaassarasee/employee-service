package com.assignment.employee.service.impl;

import com.assignment.employee.dto.user.request.LoginRequest;
import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.entity.UsersEntity;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.UnAuthorizedException;
import com.assignment.employee.mapper.UserMapper;
import com.assignment.employee.repository.UsersRepository;
import com.assignment.employee.service.JwtService;
import com.assignment.employee.service.UserService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Setter(onMethod_ = @Autowired)
@Slf4j
public class UserServiceImpl implements UserService {

    private UsersRepository usersRepository;

    private JwtService jwtService;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;

    @Override
    public LoginResponse authenticateUser(LoginRequest request) {

        log.info("authenticateUser, {}", request);

        String username = request.getUsername();
        String password = request.getPassword();

        UsersEntity userEntity = findByUsername(username);
        boolean isPasswordMatch = passwordEncoder.matches(password, userEntity.getPassword());
        if (!isPasswordMatch) {
            throw new UnAuthorizedException();
        }

        String jwtToken = jwtService.generateJwtToken(username);
        return userMapper.mapEntityAndTokenToLoginResponse(userEntity, jwtToken);
    }

    @Override
    public SignupResponse registerUser(SignupRequest request) {

        log.info("SignupRequest, {}", request);

        String username = request.getUsername();
        String email = request.getEmail();

        if (usersRepository.existsByUsername(username)) {
            throw new InvalidDataException("Username is already taken");
        }

        if (usersRepository.existsByEmail(email)) {
            throw new InvalidDataException("Email is already in use");
        }

        UsersEntity entity = userMapper.mapSignupRequestToUserEntity(request);
        UsersEntity savedEntity = usersRepository.save(entity);

        return userMapper.mapUserEntityToSignupResponse(savedEntity);
    }

    @Override
    public UsersEntity findByUsername(String username) {
        return usersRepository.findByUsername(username).orElseThrow(UnAuthorizedException::new);
    }
}
