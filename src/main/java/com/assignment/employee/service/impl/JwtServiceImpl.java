package com.assignment.employee.service.impl;

import com.assignment.employee.config.properties.JwtProperties;
import com.assignment.employee.service.JwtService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Setter(onMethod_ = @Autowired)
@Slf4j
public class JwtServiceImpl implements JwtService {

    private JwtProperties jwtProperties;

    @Override
    public String generateJwtToken(String username) {

        log.info("generateJwtToken for {}", username);

        Date date = new Date();

        return Jwts.builder()
            .setSubject(username)
            .setIssuedAt(date)
            .setExpiration(new Date(date.getTime() + jwtProperties.getJwtExpirationMs()))
            .signWith(SignatureAlgorithm.HS512, jwtProperties.getJwtSecret())
            .compact();
    }

    @Override
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtProperties.getJwtSecret()).parseClaimsJws(token).getBody().getSubject();
    }

    @Override
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtProperties.getJwtSecret()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
