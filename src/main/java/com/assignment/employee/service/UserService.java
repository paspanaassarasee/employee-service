package com.assignment.employee.service;

import com.assignment.employee.dto.user.request.LoginRequest;
import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.entity.UsersEntity;

public interface UserService {

    LoginResponse authenticateUser(LoginRequest request);

    SignupResponse registerUser(SignupRequest request);

    UsersEntity findByUsername(String username);
}
