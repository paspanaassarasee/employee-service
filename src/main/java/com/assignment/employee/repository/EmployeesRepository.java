package com.assignment.employee.repository;

import com.assignment.employee.entity.EmployeesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeesRepository extends JpaRepository<EmployeesEntity, Integer> {

}