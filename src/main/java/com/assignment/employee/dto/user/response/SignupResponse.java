package com.assignment.employee.dto.user.response;

import lombok.Data;

@Data
public class SignupResponse {
    private String username;
    private String email;
}
