package com.assignment.employee.dto.user.response;

import lombok.Data;

@Data
public class LoginResponse {
    private String token;
    private String username;
    private String email;
}
