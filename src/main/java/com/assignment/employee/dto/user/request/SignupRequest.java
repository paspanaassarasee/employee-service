package com.assignment.employee.dto.user.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SignupRequest {

    @Size(min = 1, max = 20, message = "username must be between 1 and 20")
    @NotEmpty(message = "username is required")
    private String username;

    @Size(min = 1, max = 50, message = "email must be between 1 and 50")
    @NotEmpty(message = "email is required")
    @Email
    private String email;

    @Size(min = 1, max = 16, message = "password must be between 1 and 16")
    @NotEmpty(message = "password is required")
    private String password;
}
