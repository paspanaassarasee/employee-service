package com.assignment.employee.dto.employee.response;

import lombok.Data;

import java.util.Date;

@Data
public class EmployeeResponse {
    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String departmentName;
    private Date createDate;
    private Date modifyDate;
}
