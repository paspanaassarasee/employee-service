package com.assignment.employee.dto.employee.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class SaveEmployeeRequest {

    @Size(min = 1, max = 255, message = "first name must be between 1 and 255")
    @NotEmpty(message = "first name is required")
    private String firstName;

    @Size(min = 1, max = 255, message = "last name must be between 1 and 255")
    @NotEmpty(message = "last name is required")
    private String lastName;

    @Size(min = 1, max = 255, message = "department name must be between 1 and 255")
    @NotEmpty(message = "department name is required")
    private String departmentName;
}
