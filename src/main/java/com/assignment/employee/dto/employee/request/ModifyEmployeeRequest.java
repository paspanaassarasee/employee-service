package com.assignment.employee.dto.employee.request;

import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.stream.Stream;

@Data
public class ModifyEmployeeRequest {

    @Size(min = 1, max = 255, message = "first name must be between 1 and 255")
    private String firstName;

    @Size(min = 1, max = 255, message = "last name must be between 1 and 255")
    private String lastName;

    @Size(min = 1, max = 255, message = "department name must be between 1 and 255")
    private String departmentName;

    public boolean hasAnyDetailsEntered() {
        return Stream.of(this.firstName, this.lastName, this.departmentName)
            .anyMatch(Objects::nonNull);
    }
}
