package com.assignment.employee.dto;

import lombok.Data;

@Data
public class ErrorResponse {
    private String errorCode;
    private String message;
}