package com.assignment.employee.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super();
    }

    public NotFoundException(Throwable e) {
        super(e);
    }
}