package com.assignment.employee.exception;

import lombok.Getter;

@Getter
public class InvalidDataException extends RuntimeException {
    private final String[] messages;

    public InvalidDataException(String... messages) {
        super(String.join(", ", messages));
        this.messages = messages;
    }
}
