package com.assignment.employee.exception.handler;

import com.assignment.employee.dto.ErrorResponse;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.NotFoundException;
import com.assignment.employee.exception.UnAuthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException ex) {
        log.error("Handle not found exception", ex);
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(buildErrorResponse("EMP-404", status));
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error("Handle MethodArgumentNotValidException", ex);
        HttpStatus status = HttpStatus.BAD_REQUEST;
        String[] messages = ex.getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toArray(String[]::new);
        return ResponseEntity.status(status).body(buildErrorResponse("EMP-400", status, String.join(", ", messages)));
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ErrorResponse> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        log.error("Handle MethodArgumentTypeMismatchException", ex);
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(buildErrorResponse("EMP-400", status, "Invalid path variable"));
    }

    @ExceptionHandler(InvalidDataException.class)
    public ResponseEntity<ErrorResponse> handleRequestNotValid(Exception ex) {
        log.error("Handle request not valid", ex);
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(buildErrorResponse("EMP-400", status, ex.getMessage()));
    }

    @ExceptionHandler(UnAuthorizedException.class)
    public ResponseEntity<ErrorResponse> handleUnAuthorizedException(UnAuthorizedException ex) {
        log.error("Handle unauthorized exception", ex);
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        return ResponseEntity.status(status).body(buildErrorResponse("EMP-401", status));
    }

    private ErrorResponse buildErrorResponse(String errorCode, HttpStatus httpStatus) {
        return buildErrorResponse(errorCode, httpStatus, "");
    }

    private ErrorResponse buildErrorResponse(String errorCode, HttpStatus httpStatus, String message) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(errorCode);
        errorResponse.setMessage(message);

        if (StringUtils.isBlank(errorResponse.getMessage())) {
            errorResponse.setMessage(httpStatus.getReasonPhrase());
        }

        return errorResponse;
    }
}
