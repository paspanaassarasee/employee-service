package com.assignment.employee.config;

import com.assignment.employee.controller.interceptor.JwtTokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtTokenInterceptor())
            .addPathPatterns(getDefaultIncludePathPatterns())
            .excludePathPatterns(getDefaultExcludePathPatterns());
    }

    @Bean
    public JwtTokenInterceptor jwtTokenInterceptor() {
        return new JwtTokenInterceptor();
    }

    private List<String> getDefaultIncludePathPatterns() {
        return Collections.singletonList("/api/employees/**");
    }

    private List<String> getDefaultExcludePathPatterns() {
        return Arrays.asList("/api/auth/**", "/swagger-ui/**", "/swagger-ui.html");
    }
}
