package com.assignment.employee.service.impl;

import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.entity.EmployeesEntity;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.NotFoundException;
import com.assignment.employee.mapper.EmployeeMapper;
import com.assignment.employee.repository.EmployeesRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeesRepository employeesRepository;
    @Mock
    private EmployeeMapper employeeMapper;

    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @BeforeEach
    public void setup() {
        employeeService = new EmployeeServiceImpl();
        employeeService.setEmployeesRepository(employeesRepository);
        employeeService.setEmployeeMapper(employeeMapper);
    }

    @Test
    void when_getAllEmployee_expect_listOfEmployeeResponse() throws ParseException {

        Date date = format.parse("2021-01-01 00:00:00");

        EmployeesEntity entity = new EmployeesEntity();
        entity.setEmployeeId(1);
        entity.setFirstName("p");
        entity.setLastName("a");
        entity.setDepartmentName("it");
        entity.setCreateDate(date);
        entity.setModifyDate(date);

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("p");
        employeeResponse.setLastName("a");
        employeeResponse.setDepartmentName("it");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeesRepository.findAll()).thenReturn(Collections.singletonList(entity));
        when(employeeMapper.mapEntityToResponse(any(EmployeesEntity.class))).thenReturn(employeeResponse);

        List<EmployeeResponse> employeeResponses = employeeService.getAllEmployee();
        assertEquals(Collections.singletonList(employeeResponse), employeeResponses);

        verify(employeesRepository, times(1)).findAll();
        verify(employeeMapper, times(1)).mapEntityToResponse(entity);
    }

    @Test
    void when_getAllEmployeeButEmpty_expect_emptyList() {
        when(employeesRepository.findAll()).thenReturn(Collections.emptyList());

        List<EmployeeResponse> employeeResponses = employeeService.getAllEmployee();
        assertTrue(employeeResponses.isEmpty());

        verify(employeesRepository, times(1)).findAll();
        verify(employeeMapper, never()).mapEntityToResponse(any(EmployeesEntity.class));
    }

    @Test
    void when_getEmployeeById_expect_employeeResponse() throws ParseException {
        Date date = format.parse("2021-01-01 00:00:00");

        EmployeesEntity entity = new EmployeesEntity();
        entity.setEmployeeId(1);
        entity.setFirstName("p");
        entity.setLastName("a");
        entity.setDepartmentName("it");
        entity.setCreateDate(date);
        entity.setModifyDate(date);

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("p");
        employeeResponse.setLastName("a");
        employeeResponse.setDepartmentName("it");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeesRepository.findById(anyInt())).thenReturn(Optional.of(entity));
        when(employeeMapper.mapEntityToResponse(any(EmployeesEntity.class))).thenReturn(employeeResponse);

        EmployeeResponse response = employeeService.getEmployeeById(1);

        assertEquals(employeeResponse, response);

        verify(employeesRepository, times(1)).findById(1);
        verify(employeeMapper, times(1)).mapEntityToResponse(entity);
    }

    @Test
    void when_getEmployeeById_butNotFound_expect_notFoundException() {
        when(employeesRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class, () -> employeeService.getEmployeeById(1));

        verify(employeesRepository, times(1)).findById(1);
        verify(employeeMapper, never()).mapEntityToResponse(any(EmployeesEntity.class));
    }

    @Test
    void when_deleteEmployeeById_expect_void() {
        employeeService.deleteEmployeeById(1);
        verify(employeesRepository, times(1)).deleteById(1);
    }

    @Test
    void when_saveEmployee_expect_employeeResponse() {

        SaveEmployeeRequest request = new SaveEmployeeRequest();
        request.setFirstName("a");
        request.setLastName("b");
        request.setDepartmentName("BA");

        EmployeesEntity entity = new EmployeesEntity();
        entity.setFirstName("a");
        entity.setLastName("b");
        entity.setDepartmentName("BA");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setFirstName("a");
        employeeResponse.setLastName("b");
        employeeResponse.setDepartmentName("BA");

        when(employeesRepository.save(any(EmployeesEntity.class))).thenReturn(entity);

        when(employeeMapper.mapSaveRequestToEntity(any(SaveEmployeeRequest.class))).thenReturn(entity);
        when(employeeMapper.mapEntityToResponse(any(EmployeesEntity.class))).thenReturn(employeeResponse);

        EmployeeResponse response = employeeService.saveEmployee(request);
        assertEquals(employeeResponse.getFirstName(), response.getFirstName());
        assertEquals(employeeResponse.getLastName(), response.getLastName());
        assertEquals(employeeResponse.getDepartmentName(), response.getDepartmentName());

        verify(employeesRepository, times(1)).save(entity);
        verify(employeeMapper, times(1)).mapSaveRequestToEntity(request);
        verify(employeeMapper, times(1)).mapEntityToResponse(entity);
    }

    @Test
    void when_modifyEmployeeWithAllFieldAreNull_expect_invalidDataException() {

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();

        assertThrows(InvalidDataException.class, () -> employeeService.modifyEmployee(1, request));

        verify(employeesRepository, never()).findById(anyInt());
        verify(employeesRepository, never()).save(any(EmployeesEntity.class));
        verify(employeeMapper, never()).updateEntityFromModifyRequest(any(ModifyEmployeeRequest.class), any(EmployeesEntity.class));
        verify(employeeMapper, never()).mapEntityToResponse(any(EmployeesEntity.class));
    }

    @Test
    void when_modifyEmployeeWithFindByIdNotFound_expect_notFoundException() {

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();
        request.setFirstName("modify");

        when(employeesRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> employeeService.modifyEmployee(1, request));

        verify(employeesRepository, times(1)).findById(1);
        verify(employeesRepository, never()).save(any(EmployeesEntity.class));
        verify(employeeMapper, never()).updateEntityFromModifyRequest(any(ModifyEmployeeRequest.class), any(EmployeesEntity.class));
        verify(employeeMapper, never()).mapEntityToResponse(any(EmployeesEntity.class));
    }

    @Test
    void when_modifyEmployeeWithFindByIdNotFound_expect_employeeResponse() {

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();
        request.setFirstName("modify");

        EmployeesEntity entity = new EmployeesEntity();
        entity.setFirstName("a");
        entity.setLastName("b");
        entity.setDepartmentName("BA");

        EmployeesEntity modifyEntity = new EmployeesEntity();
        modifyEntity.setFirstName("modify");
        modifyEntity.setLastName("b");
        modifyEntity.setDepartmentName("BA");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setFirstName("modify");
        employeeResponse.setLastName("b");
        employeeResponse.setDepartmentName("BA");

        when(employeesRepository.findById(anyInt())).thenReturn(Optional.of(entity));
        when(employeesRepository.save(any(EmployeesEntity.class))).thenReturn(modifyEntity);

        when(employeeMapper.mapEntityToResponse(any(EmployeesEntity.class))).thenReturn(employeeResponse);

        EmployeeResponse response = employeeService.modifyEmployee(1, request);
        assertEquals("modify", response.getFirstName());
        assertEquals("b", response.getLastName());
        assertEquals("BA", response.getDepartmentName());

        verify(employeesRepository, times(1)).findById(1);
        verify(employeesRepository, times(1)).save(entity);
        verify(employeeMapper, times(1)).updateEntityFromModifyRequest(request, entity);
        verify(employeeMapper, times(1)).mapEntityToResponse(modifyEntity);
    }
}
