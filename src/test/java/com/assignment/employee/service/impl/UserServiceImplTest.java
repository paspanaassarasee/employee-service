package com.assignment.employee.service.impl;

import com.assignment.employee.dto.user.request.LoginRequest;
import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.entity.UsersEntity;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.UnAuthorizedException;
import com.assignment.employee.mapper.UserMapper;
import com.assignment.employee.repository.UsersRepository;
import com.assignment.employee.service.JwtService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private UserServiceImpl userService;

    @Mock
    private JwtService jwtService;
    @Mock
    private UsersRepository usersRepository;
    @Mock
    private UserMapper userMapper;
    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setup() {
        userService = new UserServiceImpl();
        userService.setUsersRepository(usersRepository);
        userService.setUserMapper(userMapper);
        userService.setJwtService(jwtService);
        userService.setPasswordEncoder(passwordEncoder);
    }

    @Test
    void when_registerUserWithUsernameAlreadyExist_expect_invalidDataException() {

        SignupRequest request = new SignupRequest();
        request.setUsername("username");
        request.setEmail("email@mail.com");
        request.setPassword("password");

        when(usersRepository.existsByUsername(anyString())).thenReturn(true);

        InvalidDataException exception = assertThrows(InvalidDataException.class, () -> userService.registerUser(request));
        assertEquals("Username is already taken", exception.getMessage());

        verify(usersRepository, times(1)).existsByUsername("username");
        verify(usersRepository, never()).existsByEmail(anyString());
        verify(usersRepository, never()).save(any(UsersEntity.class));
        verify(userMapper, never()).mapSignupRequestToUserEntity(any(SignupRequest.class));
        verify(userMapper, never()).mapUserEntityToSignupResponse(any(UsersEntity.class));
    }

    @Test
    void when_registerUserWithEmailAlreadyExist_expect_invalidDataException() {

        SignupRequest request = new SignupRequest();
        request.setUsername("username");
        request.setEmail("email@mail.com");
        request.setPassword("password");

        when(usersRepository.existsByUsername(anyString())).thenReturn(false);
        when(usersRepository.existsByEmail(anyString())).thenReturn(true);

        InvalidDataException exception = assertThrows(InvalidDataException.class, () -> userService.registerUser(request));
        assertEquals("Email is already in use", exception.getMessage());

        verify(usersRepository, times(1)).existsByUsername("username");
        verify(usersRepository, times(1)).existsByEmail("email@mail.com");
        verify(usersRepository, never()).save(any(UsersEntity.class));
        verify(userMapper, never()).mapSignupRequestToUserEntity(any(SignupRequest.class));
        verify(userMapper, never()).mapUserEntityToSignupResponse(any(UsersEntity.class));
    }

    @Test
    void when_registerUser_expect_SignupResponse() {

        SignupRequest request = new SignupRequest();
        request.setUsername("username");
        request.setEmail("email@mail.com");
        request.setPassword("password");

        SignupResponse response = new SignupResponse();
        response.setUsername("username");
        response.setEmail("email@mail.com");

        UsersEntity mapperEntity = new UsersEntity();
        mapperEntity.setUsername("username");
        mapperEntity.setEmail("email@mail.com");
        mapperEntity.setPassword("encodedPassword");

        UsersEntity savedEntity = new UsersEntity();
        savedEntity.setId(1);
        savedEntity.setUsername("username");
        savedEntity.setEmail("email@mail.com");
        savedEntity.setPassword("encodedPassword");

        when(usersRepository.existsByUsername(anyString())).thenReturn(false);
        when(usersRepository.existsByEmail(anyString())).thenReturn(false);
        when(usersRepository.save(any(UsersEntity.class))).thenReturn(savedEntity);

        when(userMapper.mapSignupRequestToUserEntity(any(SignupRequest.class))).thenReturn(mapperEntity);
        when(userMapper.mapUserEntityToSignupResponse(any(UsersEntity.class))).thenReturn(response);

        SignupResponse signupResponse = userService.registerUser(request);
        assertEquals("username", signupResponse.getUsername());
        assertEquals("email@mail.com", signupResponse.getEmail());

        verify(usersRepository, times(1)).existsByUsername("username");
        verify(usersRepository, times(1)).existsByEmail("email@mail.com");
        verify(usersRepository, times(1)).save(mapperEntity);
        verify(userMapper, times(1)).mapSignupRequestToUserEntity(request);
        verify(userMapper, times(1)).mapUserEntityToSignupResponse(savedEntity);
    }

    @Test
    void when_registerAuthenticateUserWithUsernameNotExist_expect_unAuthorizedException() {

        LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");

        when(usersRepository.findByUsername(anyString())).thenThrow(UnAuthorizedException.class);

        assertThrows(UnAuthorizedException.class, () -> userService.authenticateUser(request));

        verify(usersRepository, times(1)).findByUsername("username");
        verify(passwordEncoder, never()).matches(anyString(), anyString());
        verify(jwtService, never()).generateJwtToken(anyString());
        verify(userMapper, never()).mapEntityAndTokenToLoginResponse(any(UsersEntity.class), anyString());
    }

    @Test
    void when_registerAuthenticateUserWithPasswordNotMatch_expect_unAuthorizedException() {

        LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("rawPassword");

        UsersEntity entity = new UsersEntity();
        entity.setId(1);
        entity.setUsername("username");
        entity.setEmail("email@mail.com");
        entity.setPassword("encodedPassword");

        when(usersRepository.findByUsername(anyString())).thenReturn(Optional.of(entity));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        assertThrows(UnAuthorizedException.class, () -> userService.authenticateUser(request));

        verify(usersRepository, times(1)).findByUsername("username");
        verify(passwordEncoder, times(1)).matches("rawPassword", "encodedPassword");
        verify(jwtService, never()).generateJwtToken(anyString());
        verify(userMapper, never()).mapEntityAndTokenToLoginResponse(any(UsersEntity.class), anyString());
    }

    @Test
    void when_registerAuthenticateUser_expect_loginResponse() {

        LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("rawPassword");

        UsersEntity entity = new UsersEntity();
        entity.setId(1);
        entity.setUsername("username");
        entity.setEmail("email@mail.com");
        entity.setPassword("encodedPassword");

        String jwtToken = "jwtToken";

        LoginResponse response = new LoginResponse();
        response.setToken("jwtToken");
        response.setUsername("username");
        response.setEmail("email@mail.com");


        when(usersRepository.findByUsername(anyString())).thenReturn(Optional.of(entity));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(jwtService.generateJwtToken(anyString())).thenReturn(jwtToken);
        when(userMapper.mapEntityAndTokenToLoginResponse(any(UsersEntity.class), anyString())).thenReturn(response);

        LoginResponse loginResponse = userService.authenticateUser(request);
        assertEquals("jwtToken", loginResponse.getToken());
        assertEquals("username", loginResponse.getUsername());
        assertEquals("email@mail.com", loginResponse.getEmail());

        verify(usersRepository, times(1)).findByUsername("username");
        verify(passwordEncoder, times(1)).matches("rawPassword", "encodedPassword");
        verify(jwtService, times(1)).generateJwtToken("username");
        verify(userMapper, times(1)).mapEntityAndTokenToLoginResponse(entity, jwtToken);
    }
}
