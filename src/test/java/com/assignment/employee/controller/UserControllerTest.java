package com.assignment.employee.controller;

import com.assignment.employee.dto.user.request.LoginRequest;
import com.assignment.employee.dto.user.request.SignupRequest;
import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.UnAuthorizedException;
import com.assignment.employee.service.JwtService;
import com.assignment.employee.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;
    @MockBean
    private JwtService jwtService;

    @Test
    void when_registerUserWithInvalidRequest_expect_status400() throws Exception {
        mvc.perform(post("/api/auth/signup")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\", \"email\":\"email@mail.com\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'password is required'}"));

        mvc.perform(post("/api/auth/signup")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\", \"email\":\"email@mail.com\", \"password\":\"passwordIsTooLong\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'password must be between 1 and 16'}"));

        verify(userService, never()).registerUser(any(SignupRequest.class));
    }

    @Test
    void when_registerUserWithUsernameAlreadyExist_expect_status400() throws Exception {

        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("username");
        signupRequest.setEmail("email@mail.com");
        signupRequest.setPassword("password");

        when(userService.registerUser(any())).thenThrow(new InvalidDataException("Username is already taken"));

        mvc.perform(post("/api/auth/signup")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\", \"email\":\"email@mail.com\", \"password\":\"password\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'Username is already taken'}"));

        verify(userService, times(1)).registerUser(signupRequest);
    }

    @Test
    void when_registerUser_expect_status200() throws Exception {

        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("username");
        signupRequest.setEmail("email@mail.com");
        signupRequest.setPassword("password");

        SignupResponse signupResponse = new SignupResponse();
        signupResponse.setUsername("username");
        signupResponse.setEmail("email@mail.com");

        when(userService.registerUser(any())).thenReturn(signupResponse);

        mvc.perform(post("/api/auth/signup")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\", \"email\":\"email@mail.com\", \"password\":\"password\"}"))
            .andExpect(status().isOk())
            .andExpect(content().json("{'username':'username','email':'email@mail.com'}"));

        verify(userService, times(1)).registerUser(signupRequest);
    }

    @Test
    void when_authenticateUserWithInvalidRequest_expect_status400() throws Exception {
        mvc.perform(post("/api/auth/signin")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'password is required'}"));

        verify(userService, never()).authenticateUser(any(LoginRequest.class));
    }

    @Test
    void when_authenticateUserGotUnAuthorizedException_expect_status401() throws Exception {

        LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");

        when(userService.authenticateUser(request)).thenThrow(UnAuthorizedException.class);

        mvc.perform(post("/api/auth/signin")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\",\"password\":\"password\"}"))
            .andExpect(status().isUnauthorized())
            .andExpect(content().json("{'errorCode':'EMP-401','message':'Unauthorized'}"));

        verify(userService, times(1)).authenticateUser(request);
    }

    @Test
    void when_authenticateUser_expect_status200() throws Exception {

        LoginRequest request = new LoginRequest();
        request.setUsername("username");
        request.setPassword("password");

        LoginResponse response = new LoginResponse();
        response.setToken("jwtToken");
        response.setEmail("email@mail.com");
        response.setUsername("username");


        when(userService.authenticateUser(request)).thenReturn(response);

        mvc.perform(post("/api/auth/signin")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content("{\"username\":\"username\",\"password\":\"password\"}"))
            .andExpect(status().isOk())
            .andExpect(content().json("{'token':'jwtToken','email':'email@mail.com','username':'username'}"));

        verify(userService, times(1)).authenticateUser(request);
    }
}
