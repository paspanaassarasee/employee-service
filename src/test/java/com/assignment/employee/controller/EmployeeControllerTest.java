package com.assignment.employee.controller;

import com.assignment.employee.constant.HeaderConstant;
import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.entity.UsersEntity;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.NotFoundException;
import com.assignment.employee.service.EmployeeService;
import com.assignment.employee.service.JwtService;
import com.assignment.employee.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("local")
@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmployeeService employeeService;
    @MockBean
    private JwtService jwtService;
    @MockBean
    private UserService userService;

    private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @BeforeEach
    public void setup(){
        when(jwtService.validateJwtToken(anyString())).thenReturn(true);
        when(jwtService.getUserNameFromJwtToken(anyString())).thenReturn("username");
        when(userService.findByUsername(anyString())).thenReturn(new UsersEntity());
    }

    @Test
    void when_getAllEmployee_expect_listOfEmployeeResponse() throws Exception {

        Date date = format.parse("2021-01-01 00:00:00");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("p");
        employeeResponse.setLastName("a");
        employeeResponse.setDepartmentName("it");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeeService.getAllEmployee()).thenReturn(Collections.singletonList(employeeResponse));

        mvc.perform(get("/api/employees")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isOk())
            .andExpect(content().json("[{'employeeId':1,'firstName':'p','lastName':'a','departmentName':'it','createDate':'2020-12-31T17:00:00.000+00:00','modifyDate':'2020-12-31T17:00:00.000+00:00'}]"));

        verify(employeeService, times(1)).getAllEmployee();
    }

    @Test
    void when_getAllEmployeeButEmpty_expect_emptyList() throws Exception {

        when(employeeService.getAllEmployee()).thenReturn(Collections.emptyList());

        mvc.perform(get("/api/employees")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isOk())
            .andExpect(content().json("[]"));

        verify(employeeService, times(1)).getAllEmployee();
    }

    @Test
    void when_getEmployeeById_expect_EmployeeResponse() throws Exception {

        Date date = format.parse("2021-01-01 00:00:00");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("p");
        employeeResponse.setLastName("a");
        employeeResponse.setDepartmentName("it");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeeService.getEmployeeById(anyInt())).thenReturn(employeeResponse);

        mvc.perform(get("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isOk())
            .andExpect(content().json("{'employeeId':1,'firstName':'p','lastName':'a','departmentName':'it','createDate':'2020-12-31T17:00:00.000+00:00','modifyDate':'2020-12-31T17:00:00.000+00:00'}"));

        verify(employeeService, times(1)).getEmployeeById(1);
    }

    @Test
    void when_getEmployeeByIdButNotFound_expect_status404() throws Exception {

        when(employeeService.getEmployeeById(anyInt())).thenThrow(NotFoundException.class);

        mvc.perform(get("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isNotFound())
            .andExpect(content().json("{'errorCode':'EMP-404','message':'Not Found'}"));

        verify(employeeService, times(1)).getEmployeeById(1);
    }

    @Test
    void when_getEmployeeByIdButInvalidPathVariable_expect_status400() throws Exception {

        when(employeeService.getEmployeeById(anyInt())).thenThrow(NotFoundException.class);

        mvc.perform(get("/api/employees/x")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'Invalid path variable'}"));

        verify(employeeService, never()).getEmployeeById(1);
    }

    @Test
    void when_deleteEmployeeById_expect_status200() throws Exception {

        mvc.perform(delete("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt"))
            .andExpect(status().isOk());

        verify(employeeService, times(1)).deleteEmployeeById(1);
    }

    @Test
    void when_saveEmployeeWithInvalidRequest_expect_status400() throws Exception {

        mvc.perform(post("/api/employees")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"firstName\":\"a\", \"lastName\":\"b\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'department name is required'}"));

        verify(employeeService, never()).saveEmployee(any(SaveEmployeeRequest.class));
    }

    @Test
    void when_saveEmployee_expect_status200() throws Exception {

        Date date = format.parse("2021-01-01 00:00:00");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("a");
        employeeResponse.setLastName("b");
        employeeResponse.setDepartmentName("BA");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeeService.saveEmployee(any(SaveEmployeeRequest.class))).thenReturn(employeeResponse);

        SaveEmployeeRequest request = new SaveEmployeeRequest();
        request.setFirstName("a");
        request.setLastName("b");
        request.setDepartmentName("BA");

        mvc.perform(post("/api/employees")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"firstName\":\"a\", \"lastName\":\"b\", \"departmentName\":\"BA\"}"))
            .andExpect(status().isOk())
            .andExpect(content().json("{'employeeId':1,'firstName':'a','lastName':'b','departmentName':'BA','createDate':'2020-12-31T17:00:00.000+00:00','modifyDate':'2020-12-31T17:00:00.000+00:00'}"));

        verify(employeeService, times(1)).saveEmployee(request);
    }

    @Test
    void when_modifyEmployeeButField256TooLong_expect_status400() throws Exception {

        mvc.perform(post("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"firstName\":\"82q5j9bOyl4NVSCzIvUAitSYpa9PXF2d8DP8VPI0sOyaKTEdpukxkktwFrMKrMvWg7VteTeLVtHEFVxZtyRsNPQ7WhX4Y8Y2RVANcBbtRt2aw3BO9f5Sg2Uvl29Ri9ZbNw6VIyAGqnREJIiduVxGjUa6EmCYm54UYavkcN8WYuAMqkwOZjaHRa9LeGh6hEaKhr5OSiikH7c43AIis04lUYWYli8xNmXyWkddPrk9gm55dsCt7PvWwhNl2qZtW7EC\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'first name must be between 1 and 255'}"));

        verify(employeeService, never()).modifyEmployee(anyInt(), any(ModifyEmployeeRequest.class));
    }

    @Test
    void when_modifyEmployeeButAllFieldAreNull_expect_status400() throws Exception {

        when(employeeService.modifyEmployee(anyInt(), any(ModifyEmployeeRequest.class))).thenThrow(new InvalidDataException("Invalid require field"));

        mvc.perform(post("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"fullName\":\"a\"}"))
            .andExpect(status().isBadRequest())
            .andExpect(content().json("{'errorCode':'EMP-400','message':'Invalid require field'}"));

        verify(employeeService, times(1)).modifyEmployee(1, new ModifyEmployeeRequest());
    }

    @Test
    void when_modifyEmployeeButNotFound_expect_status404() throws Exception {

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();
        request.setFirstName("a");
        request.setLastName("b");

        when(employeeService.modifyEmployee(anyInt(), any(ModifyEmployeeRequest.class))).thenThrow(NotFoundException.class);

        mvc.perform(post("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"firstName\":\"a\", \"lastName\":\"b\"}"))
            .andExpect(status().isNotFound())
            .andExpect(content().json("{'errorCode':'EMP-404','message':'Not Found'}"));

        verify(employeeService, times(1)).modifyEmployee(1, request);
    }

    @Test
    void when_modifyEmployee_expect_status200() throws Exception {

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();
        request.setFirstName("a");

        Date date = format.parse("2021-01-01 00:00:00");

        EmployeeResponse employeeResponse = new EmployeeResponse();
        employeeResponse.setEmployeeId(1);
        employeeResponse.setFirstName("modify");
        employeeResponse.setLastName("b");
        employeeResponse.setDepartmentName("BA");
        employeeResponse.setCreateDate(date);
        employeeResponse.setModifyDate(date);

        when(employeeService.modifyEmployee(anyInt(), any(ModifyEmployeeRequest.class))).thenReturn(employeeResponse);

        mvc.perform(post("/api/employees/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .header(HeaderConstant.AUTHORIZATION, "Bearer jwt")
            .content("{\"firstName\":\"a\"}"))
            .andExpect(status().isOk())
            .andExpect(content().json("{'employeeId':1,'firstName':'modify','lastName':'b','departmentName':'BA','createDate':'2020-12-31T17:00:00.000+00:00','modifyDate':'2020-12-31T17:00:00.000+00:00'}"));

        verify(employeeService, times(1)).modifyEmployee(1, request);
    }

}