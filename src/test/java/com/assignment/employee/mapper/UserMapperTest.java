package com.assignment.employee.mapper;

import com.assignment.employee.dto.user.response.LoginResponse;
import com.assignment.employee.dto.user.response.SignupResponse;
import com.assignment.employee.entity.UsersEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserMapperTest {

    @Test
    void when_mapSaveRequestToEntity_expect_entity() {
        UsersEntity entity = new UsersEntity();
        entity.setId(1);
        entity.setEmail("email@mail.com");
        entity.setPassword("password");
        entity.setUsername("username");

        UserMapper mapper = Mappers.getMapper(UserMapper.class);
        SignupResponse signupResponse = mapper.mapUserEntityToSignupResponse(entity);

        assertEquals("username", signupResponse.getUsername());
        assertEquals("email@mail.com", signupResponse.getEmail());
    }

    @Test
    void when_mapEntityAndTokenToLoginResponse_expect_loginResponse() {
        UsersEntity entity = new UsersEntity();
        entity.setId(1);
        entity.setEmail("email@mail.com");
        entity.setPassword("password");
        entity.setUsername("username");

        String token = "token";

        UserMapper mapper = Mappers.getMapper(UserMapper.class);
        LoginResponse loginResponse = mapper.mapEntityAndTokenToLoginResponse(entity, token);

        assertEquals("username", loginResponse.getUsername());
        assertEquals("email@mail.com", loginResponse.getEmail());
        assertEquals("token", loginResponse.getToken());
    }
}
