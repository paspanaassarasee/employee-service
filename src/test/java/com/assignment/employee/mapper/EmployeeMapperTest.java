package com.assignment.employee.mapper;

import com.assignment.employee.dto.employee.request.ModifyEmployeeRequest;
import com.assignment.employee.dto.employee.request.SaveEmployeeRequest;
import com.assignment.employee.dto.employee.response.EmployeeResponse;
import com.assignment.employee.entity.EmployeesEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EmployeeMapperTest {

    @Test
    void when_mapEntityToResponse_expect_response() {

        Date date = new Date();

        EmployeesEntity entity = new EmployeesEntity();
        entity.setEmployeeId(1);
        entity.setFirstName("p");
        entity.setLastName("a");
        entity.setDepartmentName("it");
        entity.setCreateDate(date);
        entity.setModifyDate(date);

        EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);
        EmployeeResponse employeeResponse = mapper.mapEntityToResponse(entity);

        assertEquals(entity.getEmployeeId(), employeeResponse.getEmployeeId());
        assertEquals(entity.getFirstName(), employeeResponse.getFirstName());
        assertEquals(entity.getLastName(), employeeResponse.getLastName());
        assertEquals(entity.getDepartmentName(), employeeResponse.getDepartmentName());
        assertEquals(entity.getCreateDate(), employeeResponse.getCreateDate());
        assertEquals(entity.getModifyDate(), employeeResponse.getModifyDate());
    }

    @Test
    void when_mapSaveRequestToEntity_expect_entity() {
        SaveEmployeeRequest request = new SaveEmployeeRequest();
        request.setFirstName("a");
        request.setLastName("b");
        request.setDepartmentName("BA");

        EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);
        EmployeesEntity entity = mapper.mapSaveRequestToEntity(request);

        assertEquals(request.getFirstName(), entity.getFirstName());
        assertEquals(request.getLastName(), entity.getLastName());
        assertEquals(request.getDepartmentName(), entity.getDepartmentName());
    }

    @Test
    void when_updateEntityFromModifyRequest_expect_entity() {

        EmployeesEntity entity = new EmployeesEntity();
        entity.setEmployeeId(1);
        entity.setFirstName("p");
        entity.setLastName("a");
        entity.setDepartmentName("it");

        ModifyEmployeeRequest request = new ModifyEmployeeRequest();
        request.setFirstName("modify");

        EmployeeMapper mapper = Mappers.getMapper(EmployeeMapper.class);

        mapper.updateEntityFromModifyRequest(request, entity);
        assertEquals("modify", entity.getFirstName());
        assertEquals("a", entity.getLastName());
        assertEquals("it", entity.getDepartmentName());
    }
}
