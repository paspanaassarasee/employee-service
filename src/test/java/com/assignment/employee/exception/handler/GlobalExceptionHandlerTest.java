package com.assignment.employee.exception.handler;

import com.assignment.employee.dto.ErrorResponse;
import com.assignment.employee.exception.InvalidDataException;
import com.assignment.employee.exception.NotFoundException;
import com.assignment.employee.exception.UnAuthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class GlobalExceptionHandlerTest {
    private GlobalExceptionHandler globalExceptionHandler;

    @BeforeEach
    public void setup() {
        globalExceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    void when_handleNotFoundException_expect_status404() {
        ResponseEntity<ErrorResponse> response = globalExceptionHandler.handleNotFoundException(new NotFoundException());

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("EMP-404", response.getBody().getErrorCode());
        assertEquals("Not Found", response.getBody().getMessage());
    }

    @Test
    void when_handleUnAuthorizedException_expect_status401() {
        ResponseEntity<ErrorResponse> response = globalExceptionHandler.handleUnAuthorizedException(new UnAuthorizedException());

        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertEquals("EMP-401", response.getBody().getErrorCode());
        assertEquals("Unauthorized", response.getBody().getMessage());
    }

    @Test
    void when_handleRequestNotValid_expect_status400() {
        ResponseEntity<ErrorResponse> response = globalExceptionHandler.handleRequestNotValid(new InvalidDataException("invalid data"));

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("EMP-400", response.getBody().getErrorCode());
        assertEquals("invalid data", response.getBody().getMessage());
    }
}
