FROM openjdk:11

COPY target/employee-0.0.1-SNAPSHOT.jar employee-0.0.1.jar

ENTRYPOINT ["java","-Dspring.profiles.active=local" ,"-jar","/employee-0.0.1.jar"]